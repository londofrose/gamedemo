﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class UIGameController : MonoBehaviour {

	public UIGameRunning uiGameRunning;
	public UIGameStart uiGameStart;

	public GameObject gameStart;
	public GameObject gameRunning;
	public GameObject gameOver;
	public GameObject gameFinish;

	public void SetUIState(GameState t){

		gameStart.SetActive (false);
		gameRunning.SetActive (false);
		gameOver.SetActive (false);
		gameFinish.SetActive (false);
		
		switch (t) {
		case GameState.Standy:
			gameStart.SetActive (true);
			break;

		case GameState.Running:
			gameRunning.SetActive (true);
			break;

		case GameState.GameOver:
			gameOver.SetActive (true);
			gameRunning.SetActive (false);
			break;

		case GameState.Finish:
			gameFinish.SetActive (true);
			gameRunning.SetActive (true);
			break;
		}

	}

	public void SetGameStart(){
		uiGameStart.SetStartImg ();

	}

}


﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class UIGameStart : MonoBehaviour {

	public Button startBtn;
	public Image startImg;

	public void SetStartImg(){
		startImg.DOFade (0.75f,1f);
		startBtn.interactable = true;
	}
}
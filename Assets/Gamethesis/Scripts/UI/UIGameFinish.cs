﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIGameFinish : MonoBehaviour {

	public Text landMarkText;
	public Text rareItemText;
	public Text coinText;
	public Text totalCoin;

	public void SetLandMarkText(int landMark,int landMarkNeed){
		landMarkText.text = landMark.ToString() + " / " + landMarkNeed.ToString();
	}

	public void SetRareItemText(int rareItem){
		rareItemText.text = rareItem.ToString ();
	}

}

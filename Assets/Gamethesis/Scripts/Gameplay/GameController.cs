﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public enum GameState{
	StartScene,
	Standy,
	Pause,
	Running,
	GameOver,
	Finish
}

public class GameController : MonoBehaviour {

	public static GameController instance;

	public GameState state;

	public UIGameController uiController;
	public GameCameraController cameraController;

	public Transform flashBackObjectPosition;
	private GameObject currentFlashBackObject;


	public int landMarkNeed;
	public int currentLandMark = 0;

	public int scoreNeed;
	public int currentScore = 0;

	void Awake(){
		instance = this;
	}

	void Start(){
		state = GameState.Standy;
		uiController.SetUIState (state);

	}

	public void AddLandMark(){
		currentLandMark++;
	}

	public void SetScore(int score){
		currentScore += score;
		uiController.uiGameRunning.SetScoreText (currentScore);

	}



	public void Finish(){
		if (currentLandMark == landMarkNeed) {
			state = GameState.Finish;
			uiController.SetUIState (state);
		} else {
			GameOver ();
		}

	}

	public void StartIntro(){
		cameraController.SetFollowCamera ();
		uiController.SetUIState (GameState.Running);
	}

	public void StartGame(){
		state = GameState.Running;
		PlayerController.instance.charState = CharacterState.Running;
	}

	public void FlashBack(GameObject flashBackObject){
		cameraController.SetFlashBackCamera ();
		currentFlashBackObject = Instantiate (flashBackObject, flashBackObjectPosition) as GameObject;

	}

	public void ReturnRunning(){
		cameraController.SetFollowCamera ();
	}

	public void GameOver(){
		state = GameState.GameOver;
		uiController.SetUIState (state);
		Debug.Log ("Game Over");
	}

	public void RestartGame(){
		SceneManager.LoadScene ("Chiangmai");
	}

	public void NextLevel(){
		SceneManager.LoadScene ("PPhuket");
	}



}

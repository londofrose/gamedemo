﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using System;

public enum CameraState{
	MapView,
	CharacterView,
	FlashBackView,
	Moving
}

public class GameCameraController : MonoBehaviour {

	public Transform playerPosition;
	public Transform flashBackPosition;
	public float cameraSpeed = 18;

	public float zoomSpeed;//set zooming speed

	public CameraState camState;

	public bool isFollow = false;

	void Start(){
		camState = CameraState.MapView;
	}

	// Update is called once per frame
	void Update ()
	{
		if (camState == CameraState.CharacterView) {
			CameraFollow ();
		} else if(camState == CameraState.FlashBackView) {

		}
		
	}

	public void SetFollowCamera(){
		transform.DOKill ();
		Debug.Log ("SetFollowCamera");
		camState = CameraState.Moving;
		transform.DOMove (playerPosition.transform.position, 1f).onComplete = () => {
			camState = CameraState.CharacterView;
			GameController.instance.StartGame();
		};
		transform.DORotate (playerPosition.localEulerAngles, 1f);
	}

	public void SetFlashBackCamera(){
		camState = CameraState.Moving;
		transform.DOMove (flashBackPosition.transform.position,1.5f);
		transform.DORotate (flashBackPosition.localEulerAngles, 1.5f).onComplete = () => {
			camState = CameraState.FlashBackView;
			PlayerController.instance.charState = CharacterState.Flashback;
		};
	}

	private void CameraFollow(){
		transform.position = Vector3.Lerp (transform.position, playerPosition.transform.position, cameraSpeed * Time.deltaTime);

	}
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public enum HitRating{
	Perfect,
	Good,
	Bad
}

public enum CharacterState{
	Idle,
	Running,
	Air,
	Talking,
	Flashback,
	Victory
}

public class PlayerController : MonoBehaviour {

	public static PlayerController instance;

	public Transform playerPosition;
	public float speed;
	public float jumpSpeed;
	public float acceleration;
	private float currentSpeed = 0;
	private float originalSpeed;

	private Vector3 dir;
	bool checkClick = true;

	private Rigidbody myRigidbody;

	
	private bool isClicked = false;

	private bool isCapturing;
	public float currentCaptureTime;
	public float captureTime = 5f;

	public int capturePoint = 1;

	public CharacterState charState;

	private Boxcontroller currentBoxController;

	void Awake(){
		instance = this;
	}

	void Start () {
		myRigidbody = GetComponent<Rigidbody> ();

		dir = Vector3.forward;
		checkClick = true;
	
		charState = CharacterState.Idle;
		originalSpeed = speed;
	}

	void Update ()
	{

		if (GameController.instance.state == GameState.Running && !isCapturing) {

			if (charState == CharacterState.Running) {
				Move ();
				CheckTap ();
			} else if (charState == CharacterState.Flashback) {
				if (Input.GetMouseButton (0)) {
					GameController.instance.ReturnRunning ();
				}
			}
				
		} else if (isCapturing){
			Capture ();

		} else if(GameController.instance.state == GameState.Finish || GameController.instance.state == GameState.GameOver) {
			myRigidbody.isKinematic = true;
		}

	}

	void Move(){
		float amountToMove = speed * Time.deltaTime;

		if (currentSpeed < amountToMove) {
			currentSpeed += acceleration * Time.deltaTime;
		} else if (currentSpeed > amountToMove) {
			currentSpeed -= acceleration * Time.deltaTime;
		}

		transform.Translate (dir * currentSpeed);
		playerPosition.position = transform.position;
	}

	void Capture(){
		currentCaptureTime += Time.deltaTime;

		if (Input.GetMouseButtonDown (0)) {
			GameController.instance.SetScore (capturePoint);
			// Add Capture Animation
		}

		if (currentCaptureTime > 5) {
			isCapturing = false;
		}
	}

	void OnTriggerEnter(Collider col)
	{
		if (GameController.instance.state == GameState.Running) {
			if (col.gameObject.tag == "tap") {
				checkClick = true;
				currentBoxController = col.GetComponent<Boxcontroller> ();

			} else if (col.gameObject.tag == "touch"){
				currentBoxController = col.GetComponent<Boxcontroller> ();
				Activate ();

			} else if (col.tag == "coin") {
				AudioSource source = GetComponent<AudioSource> ();
				source.Play ();

				Destroy (col.gameObject);
	
				GameController.instance.SetScore(5);

			} else if (col.tag == "rareitem") {
				Destroy (col.gameObject);
				//TODO add RareItem

			} else if (col.tag == "over") {
				GameController.instance.GameOver ();
			} else if (col.tag == "finish") {
				GameController.instance.Finish ();
			}
		}

	}

	void OnTriggerExit(Collider col){
		if (GameController.instance.state == GameState.Running) {
			if (col.gameObject.tag == "tap" && currentBoxController != null) {

				currentBoxController = null;

			}


		}
	}

	private void CheckTap(){
		if (Input.GetMouseButtonUp (0) && !isClicked && !isCapturing && currentBoxController != null) {
			Activate ();

		}
	}

	private void Activate(){
		float dist = Vector3.Distance (transform.position, currentBoxController.transform.position);

		BoxAction (currentBoxController);
		ShowHitRating (dist,currentBoxController);
	}

	private void BoxAction(Boxcontroller b){
		isClicked = true;
		StartCoroutine (DelayClick ());

		Debug.Log ("TAP " + b.thisAction.ToString ());

		float yRot = transform.localEulerAngles.y;
		bool isJump = false;

		Vector3 targetPos = Vector3.zero;

		b.ActiveBox ();

		switch (b.thisAction) {

		case Boxcontroller.BoxAction.Forward:
			yRot = 0;
			transform.DOMoveX (b.transform.position.x,0.3f);
			transform.DOMoveY (b.transform.position.y,0.3f);
			break;

		case Boxcontroller.BoxAction.Right:
			transform.DOMoveY (b.transform.position.y,0.3f);
			transform.DOMoveZ (b.transform.position.z,0.3f);
			yRot = 90;
			break;

		case Boxcontroller.BoxAction.Left:
			transform.DOMoveY (b.transform.position.y,0.3f);
			transform.DOMoveZ (b.transform.position.z,0.3f);
			yRot = -90;
			break;

		case Boxcontroller.BoxAction.Back:
			transform.DOMoveX (b.transform.position.x,0.3f);
			transform.DOMoveY (b.transform.position.y,0.3f);
			yRot = 180;
			break;

		case Boxcontroller.BoxAction.Jump:
			isJump = true;
			break;

		case Boxcontroller.BoxAction.SetSpeed:
			speed = b.speedAmount;
			break;

		case Boxcontroller.BoxAction.OriginalSpeed:
			speed = originalSpeed;
			break;

		case Boxcontroller.BoxAction.Capture:
			isCapturing = true;
			break;

		case Boxcontroller.BoxAction.Landmark:
			if (!b.isShowed) {
				GameController.instance.FlashBack(b.flashBack);
				charState = CharacterState.Idle;
				GameController.instance.AddLandMark ();
				b.isShowed = true;

			}

			break;
		}

		if (isJump) {
			myRigidbody.AddForce (new Vector3 (0, jumpSpeed, 0));
		} else {
			transform.localEulerAngles = new Vector3 (transform.rotation.x, yRot, transform.rotation.z);
		}
	}

	private void ShowHitRating(float dist,Boxcontroller b){
//		HitRating currentHitRating = ScoreTap (dist);
//
//		GameObject spawnObject = perfectText;
//
//		if (currentHitRating == HitRating.Perfect) {
//			spawnObject = perfectText;
//		} else if (currentHitRating == HitRating.Good) {
//			spawnObject = goodText;
//		} else if (currentHitRating == HitRating.Bad) {
//			spawnObject = badText;
//		}
//
//		GameObject hitRateText = Instantiate (spawnObject, col.transform) as GameObject;
//		hitRateText.transform.position = new Vector3 (hitRateText.transform.position.x, hitRateText.transform.position.y + 10, hitRateText.transform.position.z);
//		hitRateText.SetActive (true);
//		hitRateText.transform.localScale = new Vector3 (5, 3, 4);
//
//		Destroy (hitRateText, 0.9f);
//
//		Debug.Log (currentHitRating.ToString());

	}

	private HitRating ScoreTap(float dist){

		if (dist < 0.4f) {
			return HitRating.Perfect;

		} else if (dist < 0.6f) {
			return HitRating.Good;

		} else {
			return HitRating.Bad;

		}


	}


	IEnumerator DelayClick(){
		yield return new WaitForSeconds (0.0f);
		isClicked = false;
	}


}

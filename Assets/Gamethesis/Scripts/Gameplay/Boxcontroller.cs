﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class Boxcontroller : MonoBehaviour {

	public enum BoxAction
	{
		Left,
		Right,
		Forward,
		Back,
		Capture,
		Jump,
		Landmark,
		SetSpeed,
		OriginalSpeed
	}
	public BoxAction thisAction;

	public GameObject flashBack;
	public bool isShowed;

	public float speedAmount;

	public Material thisMat;

	private Material instanceMat;
	private Renderer thisRenderer;

	void Start(){
		thisRenderer = GetComponent<Renderer> ();
		Shader s = thisRenderer.material.shader;
		instanceMat = new Material (s);
		thisRenderer.material = instanceMat;

	}

	public void ActiveBox(){
		Color originalColor = thisRenderer.material.color;

		Sequence mySequence = DOTween.Sequence();
		mySequence.Append (thisRenderer.material.DOColor (Color.green, 0.3f));
		mySequence.Append (thisRenderer.material.DOColor (originalColor, 0.3f));

	}

}
